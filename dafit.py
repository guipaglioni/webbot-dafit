from bs4 import BeautifulSoup
import requests
import lxml
contador = 1
while True:
    site = requests.get('https://www.dafiti.com.br/calcados-femininos/botas/?cat-pwa=0&campwa=0&page=%d' %contador)
    soup = BeautifulSoup(site.content,'html.parser')
    produtos = soup.find_all('a',{'class':'product-box-link is-lazyloaded image product-image-rotate'})
    for a in produtos:
        link = a['href']
        siteProdutos = requests.get(link)
        soupProdutos = BeautifulSoup(siteProdutos.content,'html.parser')
        nomeProduto = soupProdutos.find_all('h1')
        detalhesdoProduto = soupProdutos.find_all('div',{'class':'box-description'})
        informaçoes = soupProdutos.find_all('div',{'class':'box-informations'})
        preço = soupProdutos.find_all('span',{'class':'catalog-detail-price-value'})
        for b in nomeProduto:
            print(b.text)
        for e in preço:
            print('----------------PREÇO-------------------')
            print(e.text)
            print('----------------------------------------')
        for c in detalhesdoProduto:
            print(c.text)
        for d in informaçoes:
            print(d.text)
            print(10*'-')
        
    contador=contador+1